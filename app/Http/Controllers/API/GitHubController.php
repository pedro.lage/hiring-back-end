<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\GitHubUserResource;
use App\Http\Resources\GitHubUserReposResource;

class GitHubController extends Controller
{
    public function getUser($user)
    {
    	// Send the uri to resource App\Http\Resources\GitHubUserResource
    	return new GitHubUserResource($user);
    }

    public function getUserRepos($user)
    {
    	// Send the uri to resource App\Http\Resources\GitHubUserReposResource
    	return new GitHubUserReposResource($user);
    }
}
