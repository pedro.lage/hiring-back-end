<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use GuzzleHttp\Client;

class GitHubUserReposResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $client = new Client();
        try{
            $url = $client->get('https://api.github.com/users/'.$this->resource.'/repos');
            $data = json_decode($url->getBody());
            $repos = array();

            foreach ($data as $repo) {
                $repos[] = [
                    "id" => $repo->id,
                    "name" => $repo->name,
                    "description" => $repo->description,
                    "html_url" => $repo->html_url,
                ];
            }
            return $repos;
        } catch (\Exception $err) {
            return ['error' => 'não existe usuário com este login'];
        }
    }
}
