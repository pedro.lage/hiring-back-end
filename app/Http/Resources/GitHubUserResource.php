<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use GuzzleHttp\Client;

class GitHubUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $client = new Client();
        try {
            $url = $client->get('https://api.github.com/users/'.$this->resource);
            $user = json_decode($url->getBody(), true);
            return [
                "id" => $user['id'],
                "login" => $user['login'],
                "name" => $user['name'],
                "avatar_url" => $user['avatar_url'],
                "html_url" => $user['html_url']
            ];
        } catch (\Exception $err) {
            return ['error' => 'não existe usuário com este login'];
        }

    }
}
