# GitHub API - Laravel

### Installation

Clone the project.

```sh
$ git clone https://gitlab.com/pedro.lage/hiring-back-end.git
```

Install the dependencies.

```sh
$ cd hiring-back-end
$ composer install
```

### Usage

Get user account info
```php
uri = localhost/api/users/{username}
```

Get repos from user account
```php
uri = localhost/api/users/{username}/repos
```